package Ex;

public class Circle {
    private double radius;
    private String color;

    public Circle(double r, String c) {
        radius = r;
        color = c;
    }

    public Circle() {
        radius = 1.0;
        color = "red";
    }

    public Circle(double r) {
        radius = r;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return Math.floor(Math.PI * Math.pow(radius, 2) * 100) / 100;
    }


}