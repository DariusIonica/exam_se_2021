package Ex;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestTest {

    Circle circ1 = new Circle();
    Circle circ2 = new Circle(2.0);
    Circle circ3 = new Circle(4.0,"white");

    @Test
    void getRadius() {
        assertEquals(1.0,circ1.getRadius());
        assertEquals(2.0,circ2.getRadius());
        assertEquals(4.0,circ3.getRadius());
    }

    @Test
    void getArea() {
        assertEquals(3.14, circ1.getArea());
        assertEquals(12.56,circ2.getArea());
        assertEquals(50.26, circ3.getArea());
    }

}
