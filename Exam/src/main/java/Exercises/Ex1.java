package Exercises;

//Exercise 1

public class Ex1 {
    interface  Y{
        public default void f(){
        }
    }
    class I implements  Y{
        private long t;
        private K k;

        public void f(){

        }
    }
    class K{
        public M m;
        public L l;
        K(){
            this.l = new L();
        }
    }
    class L{
        public void metA(){

        }
    }
    class M{
        public void metB(){

        }
    }
    class N{
        private I i;
    }
    class J{
        public void i(I i){

        }
    }




}
