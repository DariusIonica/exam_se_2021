package Exercises;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Ex2 implements ActionListener{
    //Exercise 2
//Implement a Java GUI application (with Swing and AWT) composed of two text fields and one button.
//The first text field holds the name of a text file. When clicking the button the number of characters from the file is written in the second text field.
        String string;
        JFrame panel = new JFrame();
        JTextField textField1 = new JTextField();
        JTextField textField2= new JTextField();
        JButton button1 = new JButton();
        StringBuilder output = new StringBuilder();

        public Ex2() {

            textField1.setBounds(50, 50, 100, 40);
            textField2.setBounds(250, 50, 100, 40);

            button1.setBounds(150, 200, 100, 25);
            button1.addActionListener(this);

            panel.add(textField1);
            panel.add(textField2);
            panel.add(button1);
            panel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            panel.setSize(400, 400);
            panel.setLayout(null);
            panel.setVisible(true);

        }

        public static void main(String[] args) {
            Ex2 exam = new Ex2();
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == button1) {
                try {
                    File reader = new File(textField1.getText()+".txt");
                    Scanner MyReader = null;
                    MyReader = new Scanner(reader);
                    while(MyReader.hasNextLine()){
                        string = MyReader.nextLine();
                    }
                    MyReader.close();
                } catch (FileNotFoundException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                }
                textField2.setText(String.valueOf(string.length()));
            }
        }
    }

